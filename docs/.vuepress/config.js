module.exports = {
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on Gitlab Pages',
    base: '/vuepress-gl-pages/',
    dest: 'public'
}